from src.text_clf import run_baseline
from src.text_clf.utils import get_argparse

if __name__ == "__main__":
    # argument parser
    parser = get_argparse()
    args = parser.parse_args()
    # Train and Test baseline model
    run_baseline(path_to_config=args.path_to_config)
