matplotlib>=3.3.4
numpy >= 1.19.5
pandas>=1.1.5
pymorphy2>=0.9.1
PyYAML>=5.4.1
scikit-learn>=0.24.2
scipy >= 1.5.4
mlflow==2.4.0
boto3==1.26.147