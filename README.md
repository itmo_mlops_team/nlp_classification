## NLP Classification
Pipeline for fast solving nlp classification task using TF-IDF + Logistic Regression approach.

## Usage
Instead of writing custom code for specific text classification task, you could:


1. install poetry:
```shell script
pip install poetry
```
2. install packages
```shell script
poetry install
```

3. (optional) download sample data from **terminal**:
```shell script
python data/load_20newsgroups.py
```
4. run pipeline in **terminal**:
```shell script
python main.py --path_to_config config.yaml
```

No data preparation is needed, only a **csv** file with two raw columns (with arbitrary names):
- `text`
- `target`

The **target** can be presented in any format, including text - not necessarily integers from *0* to *n_classes-1*.

### Config
The user interface consists of two files:
- **config.yaml** - general configuration with sklearn **TF-IDF** and **LogReg** parameters
- **hyperparams.py** - sklearn **GridSearchCV** parameters

Default **config.yaml**:
```yaml
seed: 42
path_to_save_folder: models
experiment_name: model

# data
data:
  train_data_path: data/train.csv
  test_data_path: data/test.csv
  sep: ','
  text_column: text
  target_column: target_name_short

# preprocessing
# (included in resulting model pipeline, so preserved for inference)
preprocessing:
  lemmatization: null  # pymorphy2

# tf-idf
tf-idf:
  lowercase: true
  ngram_range: (1, 1)
  max_df: 1.0
  min_df: 1

# logreg
logreg:
  penalty: l2
  C: 1.0
  class_weight: balanced
  solver: saga
  n_jobs: -1

# grid-search
grid-search:
  do_grid_search: false
  grid_search_params_path: hyperparams.py
```

**NOTE**: grid search is disabled by default, to use it set `do_grid_search: true`.

**NOTE**: `tf-idf` and `logreg` are sklearn [**TfidfVectorizer**](https://scikit-learn.org/stable/modules/generated/sklearn.feature_extraction.text.TfidfVectorizer.html?highlight=tfidf#sklearn.feature_extraction.text.TfidfVectorizer) and [**LogisticRegression**](https://scikit-learn.org/stable/modules/generated/sklearn.linear_model.LogisticRegression.html) parameters correspondingly, so you can parameterize instances of these classes however you want. The same logic applies to `grid-search` which is sklearn [**GridSearchCV**](https://scikit-learn.org/stable/modules/generated/sklearn.model_selection.GridSearchCV.html#sklearn.model_selection.GridSearchCV) parametrized with **hyperparams.py**.

### Output
After training the model, the pipeline will return the following files:
- `model.joblib` - sklearn pipeline with TF-IDF and LogReg steps
- `target_names.json` - mapping from encoded target labels from *0* to *n_classes-1* to it names
- `config.yaml` - config that was used to train the model
- `hyperparams.py` - grid-search parameters (if grid-search was used)
- `logging.txt` - logging file


### Additional functions
- `text_clf.token_frequency.get_token_frequency(path_to_config)` - <br> get token frequency of **train dataset** according to the config file parameters

**Only for binary classifiers**:
- `text_clf.pr_roc_curve.get_precision_recall_curve(path_to_model_folder)` - <br> get *precision* and *recall* metrics for precision-recall curve
- `text_clf.pr_roc_curve.get_roc_curve(path_to_model_folder)` - <br> get *false positive rate (fpr)* and *true positive rate (tpr)* metrics for roc curve
- `text_clf.pr_roc_curve.plot_precision_recall_curve(precision, recall)` - <br> plot *precision-recall curve*
- `text_clf.pr_roc_curve.plot_roc_curve(fpr, tpr)` - <br> plot *roc curve*
- `text_clf.pr_roc_curve.plot_precision_recall_f1_curves_for_thresholds(precision, recall, thresholds)` - <br> plot *precision*, *recall*, *f1-score* curves for probability thresholds

## Requirements
Python >= 3.6