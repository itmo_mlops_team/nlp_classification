import os

from .train import train_model
from .test import test_model
import mlflow
import env_vars


def run_baseline(path_to_config: str) -> None:
    """Function to train and test baseline model with exception handler.

    Args:
        path_to_config (str): Path to config.

    Returns:
        None
    """
    if env_vars.ML_FLOW_URL:
        mlflow.set_tracking_uri(env_vars.ML_FLOW_URL)
        mlflow.set_experiment(env_vars.EXPERIMENT_NAME)
        mlflow.sklearn.autolog()
        os.environ['MLFLOW_S3_ENDPOINT_URL'] = env_vars.MLFLOW_S3_ENDPOINT_URL

        with mlflow.start_run():
            model_folder = train_model(
                path_to_config=path_to_config,
            )
            test_model(model_folder)
        autolog_run = mlflow.last_active_run()
    else:

        model_folder = train_model(
            path_to_config=path_to_config,
        )
        test_model(model_folder)



