import traceback
import click
import pandas as pd
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import classification_report, confusion_matrix
from sklearn.model_selection import GridSearchCV
from sklearn.pipeline import Pipeline

from .data import load_train_data
from .logger import close_logger, get_logger
from .save_and_load import save_model
from .utils import get_grid_search_params, prepare_dict_to_print, set_seed, get_label_encoder
from .config import get_config
from mlflow import log_dict


@click.command()
@click.argument('path_to_config')
def train_model_cli(
        path_to_config: str,
) -> str:
    print("hi")
    return train_model(path_to_config)


def train_model(
        path_to_config: str,
) -> str:
    """Function to train baseline model.

    Args:
        path_to_config (str): Path to config file

    Returns:
        str: Path to model folder.
    """
    print("HUI")
    logger = None
    config = {}
    try:
        config = get_config(path_to_config=path_to_config)

        # mkdir if not exists
        config["path_to_save_folder"].absolute().mkdir(parents=True, exist_ok=True)

        # get logger
        logger = get_logger(path_to_logfile=config["path_to_save_logfile"])

        # log config
        with open(config["path_to_config"], mode="r") as fp:
            logger.info(f"Config:\n\n{fp.read()}")

        # reproducibility
        set_seed(config["seed"])

        # load data
        logger.info("Loading data...")

        X_train, y_train = load_train_data(config)

        logger.info(f"Train dataset size: {X_train.shape[0]}")

        # label encoder
        le = get_label_encoder(config)
        y_train = le.transform(y_train)

        target_names = [str(cls) for cls in le.classes_.tolist()]
        target_names_mapping = {i: cls for i, cls in enumerate(target_names)}

        # tf-idf
        vectorizer = TfidfVectorizer(**config["tf-idf"])

        # logreg
        clf = LogisticRegression(
            **config["logreg"],
            random_state=config["seed"],
        )

        # pipeline
        pipe = Pipeline(
            [
                ("tf-idf", vectorizer),
                ("logreg", clf),
            ],
            verbose=False if config["grid-search"]["do_grid_search"] else True,
        )

        if config["grid-search"]["do_grid_search"]:
            logger.info("Finding best hyper-parameters...")

            grid_search_params = get_grid_search_params(
                config["grid-search"]["grid_search_params_path"]
            )
            grid = GridSearchCV(pipe, **grid_search_params)
            grid.fit(X_train, y_train)

            pipe = grid.best_estimator_

            logger.info(
                f"Best hyper-parameters:\n{prepare_dict_to_print(grid.best_params_)}"
            )

        else:
            logger.info("Fitting TF-IDF + LogReg model...")

            pipe.fit(X_train, y_train)

        logger.info("Done!")
        logger.info(f"TF-IDF number of features: {len(pipe['tf-idf'].vocabulary_)}")

        # metrics
        logger.info("Calculating metrics...")

        y_pred_train = pipe.predict(X_train)
        classification_report_train = classification_report(
            y_true=y_train,
            y_pred=y_pred_train,
            target_names=target_names,
        )
        conf_matrix_train = pd.DataFrame(
            confusion_matrix(
                y_true=y_train,
                y_pred=y_pred_train,
            ),
            columns=target_names,
            index=target_names,
        )
        log_dict(conf_matrix_train.to_dict(orient='dict'), "conf_matrix_train.json")

        logger.info(f"Train classification report:\n\n{classification_report_train}")
        logger.info(f"Train confusion matrix:\n\n{conf_matrix_train}\n")

        # save model
        logger.info("Saving the model...")

        save_model(
            pipe=pipe,
            target_names_mapping=target_names_mapping,
            config=config,
        )

        logger.info("Done!")
    except:  # noqa
        print(traceback.format_exc())

    if logger:
        close_logger(logger)
    return config.get("path_to_save_folder")


if __name__ == "__main__":
    train_model_cli()
