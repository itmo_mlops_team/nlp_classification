import traceback
import os
import click
import pandas as pd
from sklearn.metrics import classification_report, confusion_matrix
from .data import load_test_data
from .save_and_load import load_model
from .config import LOGGING_FILE, CONFIG_FILE

from .logger import close_logger, get_logger
from .utils import set_seed, get_label_encoder
from .config import get_config
from mlflow import log_dict


@click.command()
@click.argument('model_folder')
def test_model_cli(
    model_folder: str,
) -> None:
    return test_model(model_folder)


def test_model(
    model_folder: str
) -> None:
    """
    Args:
        model_folder (str): Path to model folder

    Returns:

    """
    logger = None
    try:
        # load config
        config = get_config(path_to_config=os.path.join(model_folder, CONFIG_FILE))

        # get logger
        logger = get_logger(path_to_logfile=os.path.join(model_folder, LOGGING_FILE))

        # log config
        with open(config["path_to_config"], mode="r") as fp:
            logger.info(f"Config:\n\n{fp.read()}")

        # reproducibility
        set_seed(config["seed"])

        logger.info("Uploading model...")
        pipe = load_model(model_folder)

        # load data
        logger.info("Loading data...")
        X_test, y_test = load_test_data(config)
        logger.info(f"Test dataset size: {X_test.shape[0]}")

        # label encoding
        le = get_label_encoder(config)
        y_test = le.transform(y_test)
        target_names = [str(cls) for cls in le.classes_.tolist()]

        y_pred_test = pipe.predict(X_test)
        classification_report_test = classification_report(
            y_true=y_test,
            y_pred=y_pred_test,
            target_names=target_names,
        )
        conf_matrix_test = pd.DataFrame(
            confusion_matrix(
                y_true=y_test,
                y_pred=y_pred_test,
            ),
            columns=target_names,
            index=target_names,
        )
        log_dict(conf_matrix_test.to_dict(orient='dict'), "conf_matrix_test.json")

        logger.info(f"Test classification report:\n\n{classification_report_test}")
        logger.info(f"Test confusion matrix:\n\n{conf_matrix_test}\n")
        logger.info("Done!")
    except:  # noqa
        print(traceback.format_exc())
    if logger:
        close_logger(logger)


if __name__ == "__main__":
    test_model_cli()