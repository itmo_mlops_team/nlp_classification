from typing import Any, Dict, Tuple

import pandas as pd


def load_data_by_type(
    config: Dict[str, Any],
    data_type: str
) -> Tuple[pd.Series,  pd.Series]:
    """Load training data.

    Args:
        config (Dict[str, Any]): Config.
        data_type (str): Show which type of data we want: train, test, val.

    Returns:
        Tuple[pd.Series, pd.Series]: X_type, y_type.
    """
    text_column = config["data"]["text_column"]
    target_column = config["data"]["target_column"]

    sep = config["data"]["sep"]
    usecols = [text_column, target_column]

    df_type = pd.read_csv(
        config["data"][f"{data_type}_data_path"],
        sep=sep,
        usecols=usecols,
    )

    X_type = df_type[text_column]
    y_type = df_type[target_column]

    return X_type, y_type


def load_train_data(
        config: Dict[str, Any]
) -> Tuple[pd.Series, pd.Series]:
    """Load training data.

    Args:
        config (Dict[str, Any]): Config.
    Returns:
        Tuple[pd.Series, pd.Series]: X_train, y_train.
    """

    X_train, y_train = load_data_by_type(config, "train")
    return X_train, y_train


def load_test_data(
    config: Dict[str, Any]
) -> Tuple[pd.Series, pd.Series]:
    """Load testing data.

    Args:
        config (Dict[str, Any]): Config.

    Returns:
        Tuple[pd.Series, pd.Series]: X_test, y_test.
    """

    X_test, y_test = load_data_by_type(config, "test")
    return X_test, y_test
